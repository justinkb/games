# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A free Warlords II clone"
HOMEPAGE="http://www.nongnu.org/lordsawar/"
DOWNLOADS="mirror://savannah/${PN}/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
LANGUAGES=( ca da de hu nl pl )
MYOPTIONS="
    sound [[ description = [ Enable sound in the game via gstreamer:1.0 ] ]]
"
#    ( linguas: ca da de hu nl pl )

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.5]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libarchive[>=0.28]
        dev-cpp/libsigc++:2
        dev-cpp/libxml++:2.6
        dev-libs/libxml2:2.0
        dev-libs/libxslt[>=1.1.20]
        gnome-bindings/glibmm:2.4
        gnome-bindings/gtkmm:3
        sound? ( media-libs/gstreamermm:1.0 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/7fb13f5c6326f72394e3ac33cca5ee00bbbb4612.patch
    "${FILES}"/87f3d6f4944a91b98e492e1927726019f2a785c4.patch
    "${FILES}"/f4effd0b2e094250521712a2ec8d5ac1a2ed6b75.patch
    "${FILES}"/${PN}-0.3.1-bigmap-cairomm.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-editor
    --enable-gls
    --enable-ghs
    --enable-nls
    --disable-sound
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( sound )

src_install() {
    default

    # workaround since locales aren't being installed
    for i in "${LANGUAGES[@]}"; do
        insinto /usr/share/locale/${i}/LC_MESSAGES
        newins po/${i}.gmo ${PN}.mo
    done
}

