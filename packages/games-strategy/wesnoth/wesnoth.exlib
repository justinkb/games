# Copyright 2009 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge cmake
require systemd-service [ systemd_files=[ wesnothd.service ] ]

export_exlib_phases src_prepare src_install

SUMMARY="The Battle for Wesnoth is a turn-based strategy game with a fantasy theme"
DESCRIPTION="
Build up a great army, gradually turning raw recruits into hardened veterans.
In later games, recall your toughest warriors and form a deadly host against
whom none can stand! Choose units from a large pool of specialists, and
hand-pick a force with the right strengths to fight well on different terrains
against all manner of opposition.

Wesnoth has many different sagas waiting to be played out. Fight to regain the
throne of Wesnoth, of which you are the legitimate heir... step into the boots
of a young officer sent to guard a not-so-sleepy frontier outpost... vanquish a
horde of undead warriors unleashed by a foul necromancer, who also happens to
have taken your brother hostage... guide a band of elvish survivors in an epic
quest to find a new home.

200+ unit types. 16 races. 6 major factions. Hundreds of years of history. The
world of Wesnoth is absolutely huge and limited only by your creativity - make
your own custom units, compose your own maps, and write your own scenarios or
even full-blown campaigns. You can also challenge up to 8 friends - or
strangers - and fight in epic multi-player fantasy battles.
"
HOMEPAGE="https://www.wesnoth.org/"

UPSTREAM_CHANGELOG="https://github.com/${PN}/${PN}/blob/${PV}/changelog.md"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/wiki/GettingStarted"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/start/$(ever range -2)/"

LICENCES="GPL-2"
SLOT="0"
LOCALISATIONS="
af ang ang@latin ar ast bg ca ca_ES@valencia cs da de el en_GB en@shaw es eo et
eu fi fr fur_IT ga gd gl he hr hu id is it ja ko la lt lv mk mr nl nb_NO pl pt
pt_BR racv ro ru sk sl sr sr@ijekavian sr@ijekavianlatin sr@latin sv tl tr uk
vi zh_CN zh_TW"
MYOPTIONS="
    dbus   [[ description = [ Support for desktop notifications. ] ]]
    mysql  [[ description = [ Enable building MP/add-ons servers with mysql support ] ]]
    openmp [[ description = [ May improve perfomance on SMP configurations, use at your own risk. ] ]]
    server-only

    ( linguas:  ${LOCALISATIONS} )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# The CMakeLists.txt appears to have an automagic dep on systemd.
# However the result of the check is never actually used anywhere.
DEPENDENCIES="
    build:
        app-doc/asciidoc [[ note = [ xhtml.xsl for translations ] ]]
        sys-devel/gettext
        virtual/pkg-config
        server-only? ( media-libs/SDL:2[>=2.0.4] [[ note = [ sdl2-config ] ]] )
    build+run:
        dev-libs/boost[>=1.50.0]
        group/wesnothd
        user/wesnothd
        mysql? ( virtual/mysql )
        openmp? ( sys-libs/libgomp:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        !server-only? (
            dev-libs/fribidi[>=0.10.9]
            media-libs/fontconfig[>=2.4.1]
            media-libs/libvorbis
            media-libs/SDL:2[>=2.0.4][X]
            media-libs/SDL_image:2[>=2.0]
            media-libs/SDL_mixer:2[>=2.0][ogg]
            media-libs/SDL_ttf:2[>=2.0.12]
            sys-libs/readline:=
            x11-libs/cairo
            x11-libs/libX11
            x11-libs/pango[>=1.22.0]
            dbus? ( sys-apps/dbus )
        )
"

BUGS_TO="kimrhh@exherbo.org ingmar@exherbo.org heirecka@exherbo.org"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_NLS:BOOL=TRUE
    # Pulls in additional deps in the case of [server-only], there's no
    # target for them and they need a running X anyway.
    -DENABLE_TESTS:BOOL=FALSE
    -DFIFO_DIR:PATH=/run/wesnothd
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    # NOTIFICATIONS don\'t matter if GAME is OFF
    "dbus NOTIFICATIONS"
    MYSQL
    "openmp OMP"
    "!server-only APPDATA_FILE"
    "!server-only DESKTOP_ENTRY"
    "!server-only GAME"
    "server-only CAMPAIGN_SERVER"
)

wesnoth_src_prepare() {
    cmake_src_prepare

    local selected_localisation i
    for i in ${LOCALISATIONS} ; do
        if option linguas:${i} ; then
            selected_localisation+="${i} "
        fi
    done
    edo echo "${selected_localisation}" > "${CMAKE_SOURCE}"/po/LINGUAS

    # TODO: fix upstream
    edo sed \
        -e "/NON_LTO_AR NAMES/s/ ar/ $(exhost --tool-prefix)ar/" \
        -e "/NON_LTO_RANLIB NAMES/s/ ranlib/ $(exhost --tool-prefix)ranlib/" \
        -i CMakeLists.txt
}

wesnoth_src_install() {
    cmake_src_install

    edo find "${IMAGE}"/ -type d -empty -delete

    install_systemd_files

    insinto /etc/conf.d
    doins "${FILES}"/systemd/wesnothd.conf

    insinto "${SYSTEMDTMPFILESDIR}"
    hereins ${PN}d.conf <<EOF
d /run/wesnothd 0700 wesnothd wesnothd -
EOF
}

