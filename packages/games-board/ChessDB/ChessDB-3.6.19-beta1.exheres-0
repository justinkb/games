# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${PV/beta1/beta-1}"
MY_PNV="${PN}-${MY_PV}"

require sourceforge [ project=${PN,,} suffix=tar.gz ]

SUMMARY="Free chess database"
DESCRIPTION="
ChessDB is a free chess database which can be used on Microsoft Windows, Linux,
Apple Macs running OS X, FreeBSD, as well as most if not all modern UNIX
versions.
"
DOWNLOADS+="
    tb4? ( mirror://sourceforge/chessdb/4-piece-tablebases.zip )
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    tb4 [[ description = [ Install 4 piece tablebases ] ]]
"

DEPENDENCIES="
    build:
        tb4? ( app-arch/unzip )
    build+run:
        dev-lang/tcl
        dev-lang/tk
"

BUGS_TO="alip@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}"/${PNV}-destdir.patch )

WORK="${WORKBASE}"/${MY_PNV}

src_configure() {
    # Not autotools
    ./configure \
        BINDIR=/usr/bin \
        COMPILE="${CXX}" \
        CC="${CC}" \
        LINK="${CXX}" \
        OPTIMIZE="${CXXFLAGS}" \
        SHAREDIR=/usr/share/chessdb \
        SOUNDSDIR=/usr/share/chessdb/sounds \
        TBDIR=/usr/share/chessdb/tablebases \
        MANDIR=/usr/share/man || die "configure failed"
}

src_install() {
    default

    if option tb4; then
        insinto /usr/share/chessdb/tablebases
        doins ../*.emd
    else
        edo rmdir "${IMAGE}"/usr/share/chessdb/tablebases
    fi
}

