# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=FeralInteractive release=${PV} suffix=tar.xz ] \
    meson \
    systemd-service

SUMMARY="Daemon and library that allows games to request optimisations to be temporarily applied"
DESCRIPTION="
GameMode was designed primarily as a stop-gap solution to problems with the Intel and AMD CPU
powersave or ondemand governors, but is now able to launch custom user defined plugins, and is
intended to be expanded further, as there are a wealth of automation tasks one might want to apply.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/dbus[>=1.0]
        sys-apps/systemd
    run:
        sys-auth/polkit:1
    suggestion:
        group/games [[
            description = [ Allow renicing as an unpriviledged user being part of the games group ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-daemon=true
    -Dwith-dbus-service-dir=/usr/share/dbus-1/services
    -Dwith-examples=false
    -Dwith-pam-group=games
    -Dwith-systemd=true
    -Dwith-systemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
    -Dwith-util=true
)

